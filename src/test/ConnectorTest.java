package test;

import exception.DatabaseException;
import org.junit.jupiter.api.*;
import sqlite.Connector;
import sqlite.SQLiteConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ConnectorTest {
    private final String ONE_NAME = "Example Entry in Table One";
    private Integer ONE_ID = null;

    private final String TWO_NAME = "Example Entry in Table Two";
    private Integer TWO_ID = null;

    private Connector connector;

    @BeforeAll
    void setUp() throws Exception {
        File location = new File(System.getProperty("user.home") + File.separator + "test.db");

        if (location.exists()) {
            if (!location.delete()) {
                throw new Exception("Failed to delete old Database");
            }
        }

        SQLiteConfiguration configuration = new SQLiteConfiguration(location.getAbsolutePath());

        configuration.addTable("CREATE TABLE tableOne (" +
                "id_one INTEGER NOT NULL," +
                "name TEXT NOT NULL DEFAULT ''," +
                "PRIMARY KEY (id_one)" +
                ");");

        configuration.addTable("CREATE TABLE tableTwo (" +
                "id_two INTEGER NOT NULL," +
                "two_name TEXT NOT NULL DEFAULT ''," +
                "rel_one INTEGER NOT NULL," +
                "PRIMARY KEY (id_two)," +
                "FOREIGN KEY (rel_one) REFERENCES tableOne(id_one)" +
                ");");

        this.connector = new Connector(configuration);
    }

    @AfterAll
    void shutdown() throws Exception {
        this.connector.close();

        File location = new File(System.getProperty("user.home") + File.separator + "test.db");

        if (location.exists()) {
            if (!location.delete()) {
                throw new Exception("Failed to delete old Database");
            }
        }
    }

    @BeforeEach
    void prepare() throws DatabaseException {
        this.connector.execute("DELETE FROM tableTwo;");
        this.connector.execute("DELETE FROM tableOne;");

        this.connector.execute("INSERT INTO tableOne (name) VALUES (?);", new String[] {this.ONE_NAME});

        this.ONE_ID = this.connector.lastInsertRowId();

        this.connector.execute("INSERT INTO tableTwo (two_name, rel_one) VALUES (?, ?);", new String[] {this.TWO_NAME, this.ONE_ID+""});

        this.TWO_ID = this.connector.lastInsertRowId();
    }

    @Test
    void selectTest() throws DatabaseException {
        List result = this.connector.execute("SELECT * FROM tableTwo LEFT JOIN tableOne ON tableTwo.rel_one = tableOne.id_one WHERE id_one=?", new String[] {this.ONE_ID+""}, true);

        assertNotNull(result);
        assertEquals(1, result.size());

        Map first = Connector.getFirst(result);

        assertEquals(this.ONE_NAME, first.get("name"));
        assertEquals(this.TWO_NAME, first.get("two_name"));
        assertEquals(this.ONE_ID+"", first.get("id_one"));
        assertEquals(this.TWO_ID+"", first.get("id_two"));
    }

    @Test
    void insertTest() throws DatabaseException {
        String name = "ExampleName insertTest";
        this.connector.execute("INSERT INTO tableOne (name) VALUES (?);", new String[] {name});

        Integer id = this.connector.lastInsertRowId();

        List result = this.connector.execute("SELECT * FROM tableOne WHERE id_one=?", new String[] {id+""}, true);

        assertNotNull(result);
        assertEquals(1, result.size());

        Map first = Connector.getFirst(result);

        assertEquals(name, first.get("name"));
        assertEquals(id+"", first.get("id_one"));
    }

    @Test
    void deleteTest() throws DatabaseException {
        this.connector.execute("DELETE FROM tableOne WHERE id_one=?", new String[] {this.ONE_ID+""});

        List result = this.connector.execute("SELECT * FROM tableOne", null, true);

        assertNotNull(result);
        assertEquals(0, result.size());
    }
}
