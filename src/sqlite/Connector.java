package sqlite;

import exception.DatabaseException;

import java.sql.*;
import java.util.*;

public class Connector {
    private SQLiteConfiguration configuration;
    private Connection connection = null;

    public Connector(SQLiteConfiguration configuration) throws DatabaseException {
        String location = configuration.getDatabaseLocation();
        if (location == null) {
            throw new DatabaseException("Invalid Database Location");
        }
        this.configuration = configuration;

        this.connect();
        this.buildTableStructure();
    }

    private void connect() throws DatabaseException {
        String url = "jdbc:sqlite:" + this.configuration.getDatabaseLocation();

        try {
            this.connection = DriverManager.getConnection(url);
        } catch(SQLException e) {
            throw new DatabaseException(e);
        }
    }

    public void close() throws DatabaseException {
        try {
            this.connection.close();
        } catch (SQLException e) {
            this.connection = null;
            throw new DatabaseException(e);
        }
        this.connection = null;
    }

    public Integer lastInsertRowId() throws DatabaseException {
        List<Map<String, String>> resultList = this.execute("SELECT last_insert_rowid();", null, true);
        Map<String, String> map = Connector.getFirst(resultList);

        String lastId = map.get("last_insert_rowid()");

        try {
            return Integer.parseInt(lastId);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static Map getFirst(List<Map<String, String>> resultList) {
        if (resultList == null || resultList.size() < 1) {
            return null;
        }
        return resultList.get(0);
    }

    private List<Map<String, String>> resultSetToHashMapList(ResultSet resultSet) throws SQLException {
        List<Map<String, String>> list = new ArrayList<>();

        ResultSetMetaData metadata = resultSet.getMetaData();
        int columnCount = metadata.getColumnCount();

        while (resultSet.next()) {
            Map<String, String> map = new HashMap<>();

            for (int column=0;column<columnCount;column++) {
                String columnValue = resultSet.getString(column+1);
                String columnName = metadata.getColumnName(column+1);
                map.put(columnName, columnValue);
            }

            list.add(map);
        }

        return list;
    }

    public void execute(String sql) throws DatabaseException {
        this.execute(sql, null, false);
    }

    public void execute(String sql, String[] arguments) throws DatabaseException {
        this.execute(sql, arguments, false);
    }

    public List<Map<String, String>> execute(String sql, String[] arguments, boolean returnResults) throws DatabaseException {
        if (this.connection == null) {
            throw new DatabaseException("Failed to establish Connection");
        }
        try {
            if (arguments != null && arguments.length > 0) {
                PreparedStatement statement = this.connection.prepareStatement(sql);
                for (int index = 0; index < arguments.length; index++) {
                    String argument = arguments[index];
                    statement.setString(index+1, argument);
                }
                if (returnResults) {
                    return this.resultSetToHashMapList(statement.executeQuery());
                } else {
                    statement.executeUpdate();
                }
            } else {
                Statement statement = this.connection.createStatement();
                if (returnResults) {
                    return this.resultSetToHashMapList(statement.executeQuery(sql));
                } else {
                    statement.execute(sql);
                }
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
        return null;
    }

    private void buildTableStructure() throws DatabaseException {
        List<String> tables = this.configuration.getTables();

        for (String tableSQL : tables) {
            this.execute(tableSQL);
        }
    }

    public static void printResultList(List<Map<String, String>> list) {
        if (list == null) {
            System.out.println("null");
            return;
        }
        if (list.size() == 0) {
            System.out.println("Empty Result List");
            return;
        }
        for (int index = 0; index < list.size(); index++) {
            Map<String, String> map = list.get(index);
            Map.Entry<String, String>[] mapEntryArray = new Map.Entry[0];
            mapEntryArray = map.entrySet().toArray(mapEntryArray);
            String[] mapArray = new String[mapEntryArray.length];

            for (int entryIndex = 0; entryIndex < mapEntryArray.length; entryIndex++) {
                Map.Entry<String, String> entry = mapEntryArray[entryIndex];
                mapArray[entryIndex] = "'" + entry.getKey() + "': \"" + entry.getValue() + "\"";
            }

            System.out.println(index + ": [" + String.join(", ", mapArray) + "]");
        }
    }
}
