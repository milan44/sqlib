package sqlite;

import java.util.ArrayList;
import java.util.List;

public class SQLiteConfiguration {
    private String databaseLocation = null;
    private List<String> tables = new ArrayList<String>();

    public SQLiteConfiguration(String databaseLocation) {
        this.databaseLocation = databaseLocation;
    }

    public String getDatabaseLocation() {
        return databaseLocation;
    }

    public void setDatabaseLocation(String databaseLocation) {
        this.databaseLocation = databaseLocation;
    }

    public void addTable(String sql) {
        this.tables.add(sql);
    }

    public List<String> getTables() {
        return this.tables;
    }
}
